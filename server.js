const Pool = require('pg').Pool
const express = require('express')
const app = express()
const bodyParser = require('body-parser')

var config = {
  host: 'localhost',
  user: 'postgres',
  password: '0000',
  database:'postgres',
}
var pool = new Pool(config)

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE")
  next()
})

app.get('/api/members',(req,res)=>{
  pool.connect((err,db,done)=>{
    if(err){
      return console.log(err)
    }else{
      db.query("select * from employees",(err,result)=>{
        if(err){
          return console.log(err)
        }else{
         var data = result.rows
          return res.json(data)
        }
      })
    }
  })
})

app.post('/api/add',(req,res)=>{
var surname = req.body.surname
var age = req.body.age
var data = [surname,age]

pool.connect((err,db,done) => {
  if(err) {
    return res.status(400).send(err)
  }
  else{
    db.query('insert into employees (surname,age) values($1,$2)',[...data]
    ,(err,result) => {
      done()
      if(err){
        return res.status(400).send(err)
      }else{
        console.log('Insert '+surname)
        
        
      }
    })
  }
})
})

app.delete('/api/remove/:surname',(req,res) =>{
  var name = req.params.surname
  pool.connect((err,db,done) => {
    db.query('DELETE FROM employees WHERE surname=$1',[name],(err,result) => {
      if(err){
        return res.status(400).send(err)
      }else{
        
        return console.log('Deleted')
        
      }
    })
  })
})

app.put('/api/update/:surname',(req,res) => {
  var surname = req.body.surname
  var age = req.body.age
  var dataset = [surname,age]
  pool.connect((err,db,done) => {
    db.query('Update employees set surname=$1,age=$2 where surname=$1',[...dataset],
    (err,result => {
      if(err){
        return res.send(err)
      }else{
        return console.log('Updated !')
      }
    }))
  })

})

app.listen(3000,()=>{
  console.log('App is listening at port 3000')
})

