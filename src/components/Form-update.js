import React from 'react'


export default class Form_update extends React.Component{

    constructor(props){
        super(props)
        this.state = {items : this.props.items}
    }

    UpdateMember(e){
        e.preventDefault();
        let data = {
            surname : this.refs.surname.value,
            age : this.refs.age.value
        }
        var request = new Request('http://localhost:3000/api/update/'+data.age,{
            method:'PUT',
            headers: new Headers({'Content-Type' : 'application/json'}),
            body:JSON.stringify(data)
        }) 

        fetch(request)
        
    }
    
    render(){
        return(
            <div>
                <form ref='input-member'>
                    <input type='text' ref='surname' placeholder={this.state.items.surname}></input>
                    <input type='text' ref='age' placeholder={this.state.items.age}></input>
                    <button onClick={(e)=>this.UpdateMember(e)} type='submit'>Update</button>
                </form>
               
            </div>
        )
    }
   
}