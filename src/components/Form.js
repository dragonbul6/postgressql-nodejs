import React from 'react'


export default class Form extends React.Component{

    addMember(e){
        e.preventDefault();
        let data = {
            surname : this.refs.surname.value,
            age : this.refs.age.value
        }
        var request = new Request('http://localhost:3000/api/add',{
            method:'POST',
            headers: new Headers({'Content-Type' : 'application/json'}),
            body:JSON.stringify(data)
        }) 

        fetch(request)
        console.log(data)
    }
    
    render(){
        return(
            <div>
                <form ref='input-member'>
                    <input type='text' ref='surname' placeholder='surname'></input>
                    <input type='text' ref='age' placeholder='age'></input>
                    <button onClick={(e)=>this.addMember(e)} type='submit'>add</button>
                </form>
               
            </div>
        )
    }
   
}