import React from 'react'
import {Link} from 'react-router-dom'
import Form from './Form'

export default class Main extends React.Component{

    constructor(props){
        super(props)
        this.state = {add:false}
    }
   
    renderfrom(){
        if(this.state.add){
            return(<Form />)
        }
    }
    render(){
        return(
            <div>
                <header>
                    This is Mainpage!
                </header>
           
            <main>
                <pre>

                Test for React-Router and expressjs<br></br>

                <Link to='/members'>Members</Link> | <Link onClick={()=>this.setState({add:!this.state.add})}>Add a member</Link><br>
                </br>
                {this.renderfrom()}

                </pre>

            </main>

            <footer>
                for education
            </footer>
            </div>
        )
    }
}