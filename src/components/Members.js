import React from 'react'
import {Link} from 'react-router-dom'
import Form_update from './Form-update'



export default class Members extends React.Component{

    constructor(props){
    super(props)
    this.state = {err : '',data:[],status:false,items:[]}
}

    componentDidMount(){
        fetch('http://localhost:3000/api/members')
        .then(res => res.json())
        .then((res) =>{
            this.setState({data : res})
            console.log(this.state.data)
        },
        (err)=>{
            console.log(err)
            this.setState({err : 'have an error , look at console'})
        })
    }
    
    // findName(name){
    //     let array = this.state.data
    //     for(var i = 0 ; i < array.length ;i++){
    //         if(array[i].surname === name){
    //             return i
    //         }
    //     }
    // }
    
    delete(items){
        var request = new Request('http://localhost:3000/api/remove/'+items.surname,{
            method:'delete'
        })
        fetch(request)
        
    }
    
    renderfrom(){
        if(this.state.status){
            return (
                <div>
                <br></br>
                <Form_update items={this.state.items} />
                </div>
            )
        }
    }
    
    render(){
        let data = this.state.data
        return(
            <div>
                <header>
                    This is Members!
                </header>
           
            <main>
                
                Members List
                <ul>
                    {data.map((items) => <li>{items.surname} | {items.age} | <Link onClick={()=>this.delete(items)}>Remove</Link> | 
                    <Link onClick={()=>this.setState({status:!this.state.status,items:items})}>Update</Link>
                    
                    </li>  )}
                    {this.renderfrom()}
                </ul>
                <Link to='/'>Home</Link>
            </main>

            <footer>
                for education
            </footer>
            </div>
        )
    }
}