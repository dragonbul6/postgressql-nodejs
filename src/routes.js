import React from 'react'
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom'

import Main from './components/Mainpage'
import Members from './components/Members'

export default function AppRouter(){
    return (
        <Router>
        <Switch>
            <Route path="/" exact component={Main} />
            <Route path="/members/" component={Members} />
        </Switch>
        </Router>
    )
}

